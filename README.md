### 1. How to run project 
Go to project folder and run these commands:
```
npm install
npm start
```
Site will be lived on http://localhost:3000

### 2. How to test
#### 2.1 Unit test
Unit test uses Jest/Enzymp to test components, each component has a file *.test.js in same folder. Run these commands to implement testcases:
```
npm install
npm run unit-test
```

#### 2.2 Integration test
Integration test uses Cypress. Test specs are written in /src/__integration_test\__/*.spec.js. Run these commands to implement testcases:
```
npm install
npm run integration_test
```

Note: If node_modules was installed, please ignore command ```npm install```