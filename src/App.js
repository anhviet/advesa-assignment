import React from 'react';
import './App.css';
import ListPost from './screen/ListPost';

function App() {
  return (
    <div className="App">
      <ListPost />
    </div>
  );
}

export default App;
