describe('Go to page and load posts', () => {
  it('loading posts', () => {
    cy.visit('http://localhost:3000')
    cy.wait(3000)
    cy.get('.filter-container').should('be.visible')
    cy.get('.MuiGrid-container .post-container').should('have.length', 100)
  })
})