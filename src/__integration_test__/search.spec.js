describe('Go to page and search posts', () => {
  it('loading posts, then search post', () => {
    cy.visit('http://localhost:3000')
    cy.wait(3000)
    cy.get('.filter-container').should('be.visible')
    cy.get('.MuiGrid-container .post-container').should('have.length', 100)
    cy.get('.filter-input > input').type('Leanne Graham').should('have.value', 'Leanne Graham')
    cy.wait(300)
    cy.get('.MuiGrid-container .post-container').should('have.length', 10)
  })
})