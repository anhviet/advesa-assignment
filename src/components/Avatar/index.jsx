import React from 'react';
import PropTypes from 'prop-types'
import './style.scss'
import Avatar from '@material-ui/core/Avatar';

export default class UserAvatar extends React.Component {
  render() {
    const { src, data } = this.props
    const { name, email, address } = data
    return (
      <div className="avatar-container">
        <Avatar
          className="avatar"
          alt="User's avatar"
          src={src ? src : '/static/user-default.png'} />
        <div className="name">
          <label>{name}</label>
          <div>{email} | {address && address.city ? address.city : 'Unknown'}</div>
        </div>
      </div>
    )
  }
}

UserAvatar.propTypes = {
  data: PropTypes.object,
  src: PropTypes.string
}

UserAvatar.defaultProps = {
  data: {},
  src: '/static/user-default.png'
}