import React from 'react';
import { mount } from 'enzyme';
import UserAvatar from './index';

const data = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
  address: {
    street: "Kulas Light",
    suite: "Apt. 556",
    city: "Gwenborough",
    zipcode: "92998-3874",
    geo: {
      "lat": "-37.3159",
      "lng": "81.1496"
    }
  },
  phone: "1-770-736-8031 x56442",
  website: "hildegard.org",
  company: {
    name: "Romaguera-Crona",
    catchPhrase: "Multi-layered client-server neural-net",
    bs: "harness real-time e-markets"
  }
}

it('render avatar, no image src', () => {
  const avatar = mount(<UserAvatar data={data} />)
  expect(avatar.find('img').prop('src')).toEqual('/static/user-default.png')
  expect(avatar.find('.name > label').contains(data.name)).toEqual(true)
  expect(avatar.find('.name > div').contains(data.email)).toEqual(true)
})

it('render avatar, image src', () => {
  const avatar = mount(<UserAvatar data={data} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKsaDridr6CIghDv1rmbeG4vJzuDlv-R5V2wS_2aFdWvoC-c-ReA"/>)
  expect(avatar.find('img').prop('src')).toEqual('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKsaDridr6CIghDv1rmbeG4vJzuDlv-R5V2wS_2aFdWvoC-c-ReA')
  expect(avatar.find('.name > label').contains(data.name)).toEqual(true)
  expect(avatar.find('.name > div').contains(data.email)).toEqual(true)
})