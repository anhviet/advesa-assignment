import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

export default class Filter extends React.Component {
  render() {
    const { onFilter } = this.props
    return (
      <Paper className="filter-container">
        <InputBase
          onChange={(event) => onFilter(event.target.value)}
          className="filter-input"
          placeholder="Filter posts by keyword or username"
        />
        <Divider className="divider" />
        <IconButton className="search-btn">
          <SearchIcon />
        </IconButton>
      </Paper>
    );
  }
}

Filter.propTypes = {
  onFilter: PropTypes.func.isRequired
}