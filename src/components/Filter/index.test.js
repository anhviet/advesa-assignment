import React from 'react';
import { shallow } from 'enzyme';
import Filter from './index';

it('render filter', () => {
  const filter = shallow(<Filter onFilter={(value) => { }} />)
  expect(filter.find('.filter-input').prop('placeholder')).toEqual('Filter posts by keyword or username')
})