import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Paper from '@material-ui/core/Paper';
import UserAvatar from '../Avatar';
import Text from '../Text';

export default class Post extends React.Component {
  render() {
    const { user, title, body } = this.props.data
    return (
      <Paper className="post-container">
        <UserAvatar data={user} />
        <Text content={title} bold />
        <Text content={body} />
      </Paper>
    )
  }
}

Post.propTypes = {
  data: PropTypes.object
}

Post.defaultProps = {
  data: {}
}