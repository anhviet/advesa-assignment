import React from 'react';
import { shallow } from 'enzyme';
import Post from './index';

const data = {
  id: 1,
  user: {
    id: 1,
    name: "Leanne Graham",
    username: "Bret",
    email: "Sincere@april.biz",
    address: {
      street: "Kulas Light",
      suite: "Apt. 556",
      city: "Gwenborough",
      zipcode: "92998-3874",
      geo: {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    phone: "1-770-736-8031 x56442",
    website: "hildegard.org",
    company: {
      name: "Romaguera-Crona",
      catchPhrase: "Multi-layered client-server neural-net",
      bs: "harness real-time e-markets"
    }
  },
  title: 'This is title',
  body: 'This is body'
}

it('render post', () => {
  const post = shallow(<Post data={data} />)
  expect(post.find('UserAvatar').length).toBe(1)
  expect(post.find('Text').length).toBe(2)
})