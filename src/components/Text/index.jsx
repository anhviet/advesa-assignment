import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default class Text extends React.Component {
  render() {
    const { content, bold } = this.props
    const txtCls = bold ? 'text-bold' : ''
    return (
      <div className="text-container">
        <p className={txtCls}>{content}</p>
      </div>
    )
  }
}

Text.propTypes = {
  content: PropTypes.string,
  bold: PropTypes.bool
}

Text.defaultProps = {
  content: '',
  bold: false
}