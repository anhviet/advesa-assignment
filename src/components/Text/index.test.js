import React from 'react';
import { mount } from 'enzyme';
import Text from './index';

it('render normal text', () => {
  const textComponent = mount(<Text content="this is testing" />)
  expect(textComponent.contains('this is testing')).toEqual(true)
  expect(textComponent.find('p').hasClass('text-bold')).toBe(false)
})

it('render bold text', () => {
  const textComponent = mount(<Text content="this is testing" bold={true} />)
  expect(textComponent.contains('this is testing')).toEqual(true)
  expect(textComponent.find('p').hasClass('text-bold')).toBe(true)
})
