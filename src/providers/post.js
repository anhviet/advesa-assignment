import axios from 'axios';
const url = 'https://jsonplaceholder.typicode.com/posts';

export async function fetch() {
  try {
    const resp = await axios.get(url)
    return resp.data
  } catch (e) {
    throw (e)
  }
}