import axios from 'axios';
const url = 'https://jsonplaceholder.typicode.com/users';

export async function fetch() {
  try {
    const resp = await axios.get(url)
    return resp.data
  } catch (e) {
    throw (e)
  }
}