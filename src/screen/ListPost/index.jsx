import _ from 'lodash';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Post from '../../components/Post';
import Filter from '../../components/Filter';
import provider from '../../providers';

export default class ListPost extends React.Component {

  constructor(props) {
    super(props)
    this.filterPost = _.debounce(this.filterPost, 300)
  }

  state = {
    loading: false,
    allPosts: [],
    posts: []
  }

  componentWillMount = async () => {
    this.setState({
      loading: true
    })
    const users = await provider.user.fetch()
    const posts = await provider.post.fetch()
    this.setState({
      loading: false
    })
    posts.forEach((post) => {
      let user = users.find(u => u.id === post.userId)
      post.user = user
    })
    this.setState({
      allPosts: posts,
      posts
    })
  }

  filterPost = (keyword) => {
    const _clonePosts = JSON.parse(JSON.stringify(this.state.allPosts))
    let posts = _clonePosts.filter(post => {
      return post.user.name.indexOf(keyword) !== -1 || post.title.indexOf(keyword) !== -1
    })
    this.setState({
      posts
    })
  }

  render() {
    const { loading, posts } = this.state
    return (
      <>
        <Filter onFilter={this.filterPost} />
        {
          loading ?
            <div>
              <LinearProgress color="primary"/>
            </div>
            :
            <>
              <Grid container>
                <Grid item xs={12} sm={6} lg={6}>
                  {
                    posts.slice(0, posts.length / 2).map((post, index) => {
                      return <Post data={post} key={index} />
                    })
                  }
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  {
                    posts.slice(posts.length / 2).map((post, index) => {
                      return <Post data={post} key={index} />
                    })
                  }
                </Grid>
              </Grid>
            </>
        }
      </>
    )
  }
}