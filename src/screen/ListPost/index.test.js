import React from 'react';
import { shallow } from 'enzyme';
import ListPost from './index';

it('render list post screen', () => {
  const listpost = shallow(<ListPost />)
  expect(listpost.find('Filter').length).toBe(1)
})